import React, { memo } from 'react';
import PropTypes from 'prop-types';
import{ Button, withStyles} from '@material-ui/core';

const styles = { button: { padding: 16, background: 'black' } };

const Footer = ({classes, ...props }) => (
  <div {...props}>
    <Button variant="contained" color="primary" className={classes.button}>
      Hello Footer World
    </Button>
    <p>Footer!</p>
  </div>
);

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

Footer.defaultProps = {
};

export default memo(withStyles(styles)(Footer));